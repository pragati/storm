/*
 *  "THE BEER-WARE LICENSE" (Revision 42):
 *  <Pragati Sureka> wrote this file. As long as you retain this notice you
 *  can do whatever you want with this stuff. If we meet some day, and you think
 *  this stuff is worth it, you can buy me a beer in return Pragati Sureka
 */
package lk.blackout.storm.main;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.Utils;
import lk.blackout.storm.bolts.FreebaseLookupBolt;
import lk.blackout.storm.bolts.GraphLookupBolt;
import lk.blackout.storm.bolts.RankCalculatorBolt;
import lk.blackout.storm.bolts.SolrLookupBolt;
import lk.blackout.storm.bolts.TaggerBolt;
import lk.blackout.storm.spout.ActiveMQSpout;

/**
 *
 * @author lk
 */
public class BlackoutTopology {
    public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException {
        TopologyBuilder builder = new TopologyBuilder();
        
        builder.setSpout(Constants.BLACKOUT_SPOUT, new ActiveMQSpout(), 1);
        
        builder.setBolt(Constants.BOLT_TAGGER, new TaggerBolt(), 1).shuffleGrouping(Constants.BLACKOUT_SPOUT);
        
        builder.setBolt(Constants.BOLT_SOLR_LOOKUP, new SolrLookupBolt(), 1).shuffleGrouping(Constants.BOLT_TAGGER);
//        
//        builder.setBolt(Constants.BOLT_FREEBASE_LOOKUP, new FreebaseLookupBolt(), 1);
//        
//        builder.setBolt(Constants.BOLT_GRAPH_LOOKUP, new GraphLookupBolt(), 1);
//        
//        builder.setBolt(Constants.BOLT_CALCULATE_RANK, new RankCalculatorBolt(), 1);
        
        Config conf = new Config();

        if (args == null || args.length == 0) {
            conf.setDebug(true);
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("blackout-storm", conf, builder.createTopology());
            Utils.sleep(90000);
            cluster.killTopology("blackout-storm");
            cluster.shutdown();
        } else {
            conf.setNumWorkers(8);
            conf.setMaxSpoutPending(3);
            conf.setMessageTimeoutSecs(90);
            StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
        }        
        
        
    }
}
